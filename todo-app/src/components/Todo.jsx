import React, { Component } from "react";
class Todo extends Component {
  state = {};

  render() {
    const {
      todo,
      onHandleCheckbox,
      onHandleCross,
      ischecked,
      todoClassName,
      checkClassName,
    } = this.props;

    return (
      <div className={todoClassName}>
        <span className={checkClassName}>
          <img
            src="/icon-check.svg"
            alt="check"
            onClick={() => onHandleCheckbox(todo.id)}
          />
        </span>
        <p>{todo.value}</p>
        <span data-cross="cross" onClick={() => onHandleCross(todo.id)}>
          <img src="/icon-cross.svg" alt="check" key={todo.key} />
        </span>
      </div>
    );
  }
}

export default Todo;
