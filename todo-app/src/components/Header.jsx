import React, { Component } from "react";
import TodoList from "./TodoList";

class Header extends Component {
  getImgSource(modeIcon) {
    if (modeIcon === "sun") {
      return "/icon-moon.svg";
    } else if (modeIcon === "moon") {
      return "/icon-sun.svg";
    }
  }

  render() {
    const {
      modeIcon,
      onHandleModes,
      className,
      onHandleKeyPress,
      list,
      onHandleCheckbox,
      onHandleCross,
      itemsLeft,
      onHandleClear,
      onHandleAllStates,
    } = this.props;

    return (
      <header className={className}>
        <h1>
          <p className="title">TODO</p>
          <span>
            <img
              className="modeImg"
              onClick={onHandleModes}
              src={this.getImgSource(modeIcon)}
              alt="sun"
            />
          </span>
        </h1>
        <TodoList
          onHandleKeyPress={onHandleKeyPress}
          list={list}
          onHandleCheckbox={onHandleCheckbox}
          onHandleCross={onHandleCross}
          itemsLeft={itemsLeft}
          onHandleClear={onHandleClear}
          onHandleAllStates={onHandleAllStates}
        />
      </header>
    );
  }
}

export default Header;
