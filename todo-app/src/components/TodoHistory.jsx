import React, { Component } from "react";

class TodoHistory extends Component {
  render() {
    let { itemsLeft, onHandleClear } = this.props;
    return (
      <div data-history="history">
        <div data-history="items-left">{`${itemsLeft} items left`}</div>
        <div data-history="clear-completed" onClick={onHandleClear}>
          Clear-Completed
        </div>
      </div>
    );
  }
}

export default TodoHistory;
