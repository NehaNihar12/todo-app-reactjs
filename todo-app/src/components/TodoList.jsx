import React, { Component } from "react";
import Todo from "./Todo";
import TodoHistory from "./TodoHistory";
import TodoStates from "./TodoStates";

class TodoList extends Component {
  render() {
    let {
      list,
      onHandleKeyPress,
      onHandleCheckbox,
      onHandleCross,
      itemsLeft,
      onHandleClear,
      onHandleAllStates,
    } = this.props;

    return (
      <div className="container">
        <div className="todo-input">
          <input
            type="text"
            placeholder="Create a new todo..."
            onKeyPress={onHandleKeyPress}
          />
        </div>
        <div>
          {list.map((todo) => {
            return (
              <Todo
                todoClassName={
                  todo.completed === false ? "todos" : "todos line-through"
                }
                checkClassName={
                  todo.completed === false ? "checkbox" : "checkbox red"
                }
                todo={todo}
                onHandleCheckbox={onHandleCheckbox}
                onHandleCross={onHandleCross}
              />
            );
          })}
        </div>
        <TodoHistory itemsLeft={itemsLeft} onHandleClear={onHandleClear} />
        <TodoStates onHandleAllStates={onHandleAllStates} />
        <div className="footer">Drag and drop to reorder list</div>
      </div>
    );
  }
}

export default TodoList;
