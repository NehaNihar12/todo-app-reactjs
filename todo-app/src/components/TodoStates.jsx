import React, { Component } from "react";

class TodoStates extends Component {
  state = {};

  render() {
    const { onHandleAllStates } = this.props;
    return (
      <div id="states" data-states="states" onClick={onHandleAllStates}>
        <div id="all" data-states="all">
          All
        </div>
        <div id="active" data-states="active">
          Active
        </div>
        <div id="completed" data-states="completed">
          Completed
        </div>
      </div>
    );
  }
}

export default TodoStates;
