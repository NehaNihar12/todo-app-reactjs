import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header";

class App extends Component {
  constructor(props) {
    super();

    this.state = {
      modeIcon: "sun",
      bgMode: "light",
      list: [],
      checked: [],
      itemsLeft: 0,
    };
  }

  handleModes = () => {
    let mode = this.state.modeIcon;
    if (mode === "sun") {
      this.setState({
        modeIcon: "moon",
        bgMode: "dark",
      });
    } else if (mode === "moon") {
      this.setState({
        modeIcon: "sun",
        bgMode: "light",
      });
    }
  };

  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      let value = event.target.value;
      if (value === "") {
        return;
      }
      let list1 = this.state.list;
      list1.push({
        key: Date.now(),
        id: Date.now(),
        value: value,
        completed: false,
      });
      let itemsLeft = this.state.itemsLeft + 1;
      this.setState({ list: list1, itemsLeft: itemsLeft });
      event.target.value = "";
    }
  };

  handleCheckbox = (id) => {
    let itemsLeft = this.state.itemsLeft;
    const list = this.state.list.map((todo) => {
      if (todo.id === id) {
        if (todo.completed === false) {
          todo.completed = true;
          itemsLeft = itemsLeft - 1;
        } else {
          todo.completed = false;
          itemsLeft = itemsLeft + 1;
        }
      }
      return todo;
    });
    this.setState(
      {
        list: list,
        itemsLeft: itemsLeft,
      },
      () => {
        let checked = this.state.list.filter((li) => li.completed === true);
        this.setState({ checked: checked });
      }
    );
  };

  handleCross = (id) => {
    let itemsLeft = this.state.itemsLeft;
    const list = this.state.list.filter((todo) => {
      if (todo.id === id) {
        if (todo.completed === false) {
          itemsLeft -= 1;
        }
      }
      return todo.id !== id;
    });
    this.setState({ list: list, itemsLeft: itemsLeft });
  };

  handleClear = () => {
    let list = this.state.list.filter((li) => li.completed === false);
    this.setState({ list: list });
  };

  //display items in different states
  handleAllStates = (e) => {
    let items = Array.from(document.querySelectorAll(".todos"));
    let inactiveItems = items.filter((item) =>
      item.classList.contains("line-through")
    );
    let activeItems = items.filter(
      (item) => !item.classList.contains("line-through")
    );
    if (e.target.id === "active") {
      activeItems.forEach((activeItem) => (activeItem.style.display = "flex"));
      inactiveItems.forEach(
        (inactiveItem) => (inactiveItem.style.display = "none")
      );
    }
    if (e.target.id === "completed") {
      inactiveItems.forEach(
        (inactiveItem) => (inactiveItem.style.display = "flex")
      );
      activeItems.forEach((activeItem) => (activeItem.style.display = "none"));
    }
    if (e.target.id === "all") {
      inactiveItems.forEach(
        (inactiveItem) => (inactiveItem.style.display = "flex")
      );
      activeItems.forEach((activeItem) => (activeItem.style.display = "flex"));
    }
  };

  render() {
    return (
      <div className="App">
        <Header
          list={this.state.list}
          className={`${this.state.bgMode}-mode main-div`}
          onHandleModes={this.handleModes}
          modeIcon={this.state.modeIcon}
          onHandleKeyPress={this.handleKeyPress}
          onHandleCheckbox={this.handleCheckbox}
          onHandleCross={this.handleCross}
          itemsLeft={this.state.itemsLeft}
          onHandleClear={this.handleClear}
          onHandleAllStates={this.handleAllStates}
        ></Header>
      </div>
    );
  }
}

export default App;
